#+TITLE: Default Emacs Configuration

* CUSTOM FILE
#+begin_src emacs-lisp
(setq custom-file (locate-user-emacs-file "custom.el"))
(load custom-file 'noerror 'nomessage)
#+end_src

* DISABLE MESSAGES BUFFER
#+begin_src emacs-lisp
(setq-default message-log-max nil)
#+end_src

* COLOR THEME
#+BEGIN_SRC emacs-lisp
  (add-to-list 'custom-theme-load-path (expand-file-name "~/.config/emacs/themes/"))

  (setq modus-themes-mode-line '(accented borderless)
      modus-themes-bold-constructs t
      modus-themes-italic-constructs t
      modus-themes-fringes 'subtle
      modus-themes-tabs-accented t
      modus-themes-paren-match '(bold intense)
      modus-themes-prompts '(bold intense)
      modus-themes-org-blocks 'tinted-background
      modus-themes-scale-headings t
      modus-themes-region '(bg-only)) 

  (load-theme 'doom-material-dark t)
#+END_SRC

* DEFAULT SCRATCH BUFFER MESSAGE
#+begin_src emacs-lisp
(setq initial-scratch-message "")
#+end_src

* DISABLE FRINGES
#+begin_src emacs-lisp
(fringe-mode 0) 
#+end_src

* FIX GAPS ON WINDOW MANAGER 
#+begin_src emacs-lisp
 (setq frame-resize-pixelwise t)
#+end_src

* SPLASH SCREEN
#+BEGIN_SRC emacs-lisp
  (setq inhibit-splash-screen t)
#+END_SRC

* TRANSPARENCY
#+BEGIN_SRC emacs-lisp
  (set-frame-parameter nil 'alpha-background 100) 
  (add-to-list 'default-frame-alist '(alpha-background . 100)) 
#+END_SRC

* DISABLE MENUS AND SCROLLBAR
#+BEGIN_SRC emacs-lisp
  (menu-bar-mode -1)
  (tool-bar-mode -1)
  (scroll-bar-mode -1)
#+END_SRC

* CONFIGURE FONT FAMILY
#+BEGIN_SRC emacs-lisp
  (set-face-attribute 'default nil
                      :family "JetBrainsMono Nerd Font"
                      :height 100
                      :weight 'normal
                      :width 'normal)
#+END_SRC

* CONFIGURE LINE NUMBER
#+BEGIN_SRC emacs-lisp
  (add-hook 'prog-mode-hook 'display-line-numbers-mode)
  (add-hook 'org-mode-hook 'display-line-numbers-mode)
  (setq display-line-numbers-type 'relative)
#+END_SRC

* DISABLE TEXT WRAP
#+BEGIN_SRC emacs-lisp
  (setq-default truncate-lines 1)
#+END_SRC

* SET CURSOR TYPE
#+BEGIN_SRC emacs-lisp
  (setq cursor-type 'bar)
#+END_SRC

* DISABLE AUTO BACKUP FILES
#+BEGIN_SRC emacs-lisp
  (setq make-backup-files nil)
  (setq auto-save-list-file-prefix nil)
#+END_SRC

* DISABLE AUTO SAVE FILES
#+BEGIN_SRC emacs-lisp
  (setq auto-save-default nil)
#+END_SRC

* MOVE LINE UP AND DOWN
#+begin_src emacs-lisp
  (defun move-line-up ()
    "Move the current line up."
    (interactive)
    (transpose-lines 1)
    (previous-line 2))

  (defun move-line-down ()
    "Move the current line down."
    (interactive)
    (next-line 1)
    (transpose-lines 1)
    (previous-line 1))
#+end_src

* Custom header
#+begin_src emacs-lisp
  (defun custom-header ()
    "Insert a specific message at the current cursor position in the buffer."
    (interactive)
    (let ((message "
   _____   _____ ___ ___ 
  |   \\ \\ / / _ \\ _ ) __| dvrbs.org
  | |) \\ V /|   / _ \\__ \\ contato@dvrbs.org
  |___/ \\_/ |_|_\\___/___/ https://gitlab.com/dvrbs
    "))
      (insert message)))

  (global-set-key (kbd "C-c h") 'custom-header)
#+end_src

* DISABLE SCRATCH AND MESSAGES BUFFER ON STARTUP
#+begin_src emacs-lisp
(kill-buffer "*scratch*")
(kill-buffer "*Messages*")
#+end_src

* DEFAULT SHELL FOR TERMINALS
#+begin_src emacs-lisp 
(setq explicit-shell-file-name "/usr/bin/fish")
#+end_src

* SHORTCUTS
** USNSET KEYS
#+begin_comment
  (global-unset-key (kbd "C-d"))
  (global-unset-key (kbd "C-l"))
  (global-unset-key (kbd "C-y"))
  (global-unset-key (kbd "C-x C-u"))
  (global-unset-key (kbd "C-k"))
  (global-unset-key (kbd "C-w"))
  (global-unset-key (kbd "C-p"))

  (global-unset-key (kbd "M-f"))
  (global-unset-key (kbd "M-b"))
  (global-unset-key (kbd "M-x"))

  (global-unset-key (kbd "C-x"))
#+end_comment
** EXECUTE
#+begin_comment
  (global-set-key (kbd "M-SPC") 'execute-extended-command)
#+end_comment
** MOVEMENT
#+begin_comment
  (global-set-key (kbd "C-s") 'forward-char)
  (global-set-key (kbd "C-h") 'backward-char)
  (global-set-key (kbd "C-t") 'previous-line)
  (global-set-key (kbd "C-n") 'next-line)

  (global-set-key (kbd "C-f") 'forward-word)
  (global-set-key (kbd "C-b") 'backward-word)

  (global-set-key (kbd "C-l C-e") 'move-end-of-line)
  (global-set-key (kbd "C-l C-b") 'move-beginning-of-line)  

  (global-set-key (kbd "M-b") 'beginning-of-buffer)
  (global-set-key (kbd "M-f") 'end-of-buffer)
#+end_comment

** EDITING
#+begin_comment
  (global-set-key (kbd "C-d C-w") 'kill-word)
  (global-set-key (kbd "C-d C-l") 'kill-line)
  (global-set-key (kbd "C-d C-d") 'kill-whole-line)

  (global-set-key (kbd "M-c") 'kill-ring-save)
  (global-set-key (kbd "M-v") 'yank)

  (global-set-key (kbd "M-t") 'move-line-up)
  (global-set-key (kbd "M-n") 'move-line-down)
#+end_comment

** BUFFERS AND WINDOWS
#+begin_comment
  (global-set-key (kbd "C-k C-b") 'kill-buffer)
  (global-set-key (kbd "C-k C-w") 'kill-buffer-and-window)
  (global-set-key (kbd "C-w C-d") 'delete-window)
  (global-set-key (kbd "C-x C-b") 'counsel-switch-buffer)

  (global-set-key (kbd "C-w C-x") 'split-window-below)
  (global-set-key (kbd "C-w C-v") 'split-window-right)

  (global-set-key (kbd "C-w C-h") 'windmove-left)
  (global-set-key (kbd "C-w C-t") 'windmove-up)
  (global-set-key (kbd "C-w C-n") 'windmove-down)
  (global-set-key (kbd "C-w C-s") 'windmove-right)
  (global-set-key (kbd "C-w C-w") 'other-window)

  (global-set-key (kbd "C-+") 'text-scale-increase)
  (global-set-key (kbd "C--") 'text-scale-decrease)
#+end_comment

** WRITE FILE
#+begin_comment
  (global-set-key (kbd "C-S-w") 'save-buffer)
  (global-set-key (kbd "C-M-w") 'write-file)
#+end_comment
