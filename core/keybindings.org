#+TITLE: KEYBINDINGS
#+AUTHOR: dev_rubs
#+EMAIL: rubemchrist@gmail.com
#+OPTIONS: num:nil

* UNSET KEYBINDS
#+begin_src emacs-lisp
  (global-unset-key (kbd "M-k"))
  (global-unset-key (kbd "C-w"))
#+end_src

* EXECUTE
#+begin_src emacs-lisp
  (global-set-key (kbd "M-SPC") 'execute-extended-command)
#+end_src

* SHELL COMMAND
#+begin_src emacs-lisp
  (global-set-key (kbd "M-!") 'shell-command)
#+end_src

* BUFFERS

** FIND FILE
#+begin_src emacs-lisp
  (global-set-key (kbd "C-x C-f") 'counsel-find-file)
#+end_src

** SWITCH BUFFER
#+begin_src emacs-lisp
  (global-set-key (kbd "C-x C-b") 'counsel-switch-buffer)
#+end_src

** MAGINIFY BUFFER
#+begin_src emacs-lisp
  (global-set-key (kbd "C-=") 'text-scale-increase)
  (global-set-key (kbd "C--") 'text-scale-decrease)
#+end_src

** SPLIT WINDOW
#+begin_src emacs-lisp
  (global-set-key (kbd "C-w /") 'split-window-right)
  (global-set-key (kbd "C-w -") 'split-window-below)
#+end_src

* PROJECTILE
#+begin_src emacs-lisp
    (global-set-key (kbd "M-p f") 'projectile-find-file)
    (global-set-key (kbd "M-p r") 'projectile-run-project)
    (global-set-key (kbd "M-p c") 'projectile-configure-project)
#+end_src

* TREEMACS
#+begin_src emacs-lisp
  (global-set-key (kbd "M-p d") 'treemacs)
#+end_src

* LSP MODE
#+begin_src emacs-lisp
  (global-set-key (kbd "M-g d") 'lsp-find-definition)
  (global-set-key (kbd "M-g i") 'lsp-find-implementation)
  (global-set-key (kbd "M-g r") 'lsp-find-references)
  (global-set-key (kbd "M-k M-d") 'lsp-format-buffer)
#+end_src
