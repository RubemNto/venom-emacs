(org-babel-load-file "~/.config/emacs/core/default.org")
(org-babel-load-file "~/.config/emacs/core/package-manager.org")
(org-babel-load-file "~/.config/emacs/core/keybindings.org")

(org-babel-load-file "~/.config/emacs/configs/hl.org")
(org-babel-load-file "~/.config/emacs/configs/evil.org")
(org-babel-load-file "~/.config/emacs/configs/file-manager.org")
(org-babel-load-file "~/.config/emacs/configs/org.org")
(org-babel-load-file "~/.config/emacs/configs/lsp.org")
(org-babel-load-file "~/.config/emacs/configs/modeline.org")
